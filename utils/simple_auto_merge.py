#!/usr/bin/env python

# TODO:
# add -group flag
# Add TNECORR + TN DM 

from argparse import ArgumentParser
import os.path
import glob
import re

parser = ArgumentParser(prog='simple automated DR2 merger')

parser.add_argument("-p", "--psr", dest='PSR', nargs=1, help="Choose pulsar",
                    required = True)
parser.add_argument("-o", "--out_prefix", dest="prefix",
                    help="Choose output files prefix", default="ipta")
parser.add_argument("-t", "--top_path", dest ="topPath", nargs=1,
                    help="Full path to the DR2 repo", required = True)
parser.add_argument("-D","--DMMODEL", dest="DMGrid", nargs=3, type=int,
                    metavar=("DM_GRID_START", "DM_GRID_STOP", "DM_GRID_STEP"),
                    help="Create DM MODEL on a grid from DM_GRID_START to \n"
                         "DM_GRID_STOP with a point every DM_GRID_STEP\n"
                         "Currently only integer values accepted")
parser.add_argument("-J", "--jump_disable", dest="disableJUMPFitSystem", nargs=1,
                    metavar="SYSTEM", default="NRT.BON.1400",
                    help="Disable jump for the specified system.\n"
                         "Defaults to NRT.BON.1400")
# Use DR1 data:
parser.add_argument("--DR1", dest="DR1TopPath", nargs=1, 
                    help="Top path of the DR1 repository")
parser.add_argument("--TNE", dest="useTNE", action='store_true', default=False,
                    help="USE TN EFACs and EQUADs from DR1. You must specify"
                         " the top path to DR1 repository")

args = parser.parse_args()

PSR = args.PSR[0]
topPath = args.topPath[0]
prefix = args.prefix

produceDMGrid = False
if args.DMGrid:
    produceDMGrid = True
    DMGridStart = args.DMGrid[0]
    DMGridEnd   = args.DMGrid[1]
    DMGridStep  = args.DMGrid[2]

DR1TopPath = ""
useTNE = args.useTNE
if useTNE:
    if not args.DR1TopPath:
        print "You must use --DR1 when using --TNE"
        exit()
    DR1TopPath = args.DR1TopPath[0]

disableJUMPFitSystem = args.disableJUMPFitSystem

PTAs = ["EPTA", "NANOGrav", "PPTA", ]

# make possible to modify via command line?
timLocator = {
    "EPTA"     : "EPTA_v2.2/%s/tims/*tim",
    "NANOGrav" : "NANOGrav_9y/tim/%s_NANOGrav_9yv1.tim",
    "PPTA"     : "PPTA_dr1dr2/tim/v2/%s_dr1dr2.tim",
}

parLocator = {
    "EPTA"     : "EPTA_v2.2/%s/%s.par",
    "NANOGrav" : "NANOGrav_9y/par/%s_NANOGrav_9yv1.gls.par",
    "PPTA"     : "PPTA_dr1dr2/par/%s_dr1dr2.par",
}

jumpLocator = {
    "EPTA"     : "-sys",
    "NANOGrav" : "-fe",
    "PPTA"     : "-q",
}

# a list of JUMPs to be populated soon
JUMPs = []

timFileHandle = open(prefix + ".tim", "w")
timFileHandle.write("FORMAT 1" + '\n')
for PTA in PTAs:
    timFileHandle.write("# %s:" % PTA + '\n')
    inputTims = glob.glob(topPath+"/"+ timLocator[PTA] % PSR)
    inputTims.sort()
    JUMPs.append('# ' + PTA + "'s JUMPs:")
    for inputTim in inputTims:
        timFileHandle.write("INCLUDE " + os.path.relpath(inputTim) + '\n')
        # since we are looping through the tmfiles, determine JUMPs:
        inputTimHandle = open( inputTim, "r")
        jumpsInCurrentTim = ["JUMP "+jumpLocator[PTA] + " %s 0.0 %d" % (x, not disableJUMPFitSystem in x) for x in list(set(re.findall(jumpLocator[PTA] + "\s*(\S*)", open( inputTim, "r").read())))]
        for JUMP in jumpsInCurrentTim:
            JUMPs.append(JUMP)
        inputTimHandle.close()

timFileHandle.close()

# Create the par file:
excludedParams = ['DM1', 'DM2', 'JUMP', ]
with open( topPath + "/" + parLocator["EPTA"] % (PSR, PSR), "r") as baseParFileHandle, open(prefix + ".par", "w") as parFileHandle:
    # copy the EPTA par file, excluding certain parameters:
    # At the same time, check if UNITS defined
    unitsDefined = False
    for line in baseParFileHandle:
        if not any(excludedParam in line for excludedParam in excludedParams):
            parFileHandle.write(line)
        if 'UNITS' in line:
            unitsDefined = True

# should I just move everything below to the above context?
parFileHandle = open(prefix + ".par", "a")
# Add explicitely UNITS TCB if no UNITS are present in the parfile
# WARNING: this makes the parfile tempo2 specific:
if not unitsDefined:
    parFileHandle.write('UNITS TCB\n')

# Add as many FD parameters as NANOGrav uses:
FDparamsCount = 1 # Allow override from command line (argparse)?
# determine the number of FD params:
with open(topPath + "/" + parLocator["NANOGrav"] % PSR, "r") as NANOGravParFileHandle:
    for line in NANOGravParFileHandle:
        if len(re.findall('^FD[0-9]+', line)) == 1:
            FDparamsCount += 1

for i in xrange(1,FDparamsCount):
    parFileHandle.write('FD%d 0.0 1 \n' % i)

# Append the list of PPTA's fixed JUMPs:
JUMPs.append("# PPTA's fixed JUMPs:")
with open(topPath + "/utils/PPTA_fixed_JUMPs.list") as PPTAFixedJumpsFileHandle:
    for line in PPTAFixedJumpsFileHandle:
        JUMPs.append(line.strip('\n'))

parFileHandle.write('\n')
# Add JUMPs to the par file:
for JUMP in JUMPs:
    parFileHandle.write(JUMP + '\n')
parFileHandle.write('\n')

if useTNE:
    parFileHandle.write("# DR1 TNEF / TNEQ:\n")
    for TNpar in re.findall("TNE[F,Q] -group .*", open(DR1TopPath + "/VersionC/%s/%s.par" % (PSR, PSR)).read()):
        parFileHandle.write(TNpar+'\n')
    print "WARNING: TNEF / TNEQ added but -group flags not added to tim file"
    parFileHandle.write('\n')

if produceDMGrid:
    parFileHandle.write('# DM grid:\n')
    parFileHandle.write('DMMODEL DM 1\n')
    for MJD in xrange(DMGridStart, DMGridEnd, DMGridStep):
        parFileHandle.write('DMOFF %f 0 0\n' % MJD)
    parFileHandle.write('CONSTRAIN DMMODEL\n')

parFileHandle.close()
