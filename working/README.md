# Working directory

This is where we will store various filtered datasets.

## NG9
* Contains 18 pulsars from NG data from 9-year limit paper.

## NG9 + 0437
* Contains 18 pulsars from NG data from 9-year limit paper and J0747-4715 from PPTA.

## NG9 + PPTA4
* Contains 18 pulsars from NG data from 9-year limit paper + 4 pulsars
from Shannon et al (2016) paper.
* J1713+0747, J1744-1134, J1909-3744 contain both NG9 and PPTA data

## NG9 - PPTA4  + PPTA4
* Contains 15 pulsars from NG data from 9-year limit paper + 4 pulsars
from Shannon et al (2016) paper.
* J1713+0747, J1744-1134, J1909-3744, and J0437-4715 contain only PPTA data
